package com.ninja.alarmclock

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.service.notification.StatusBarNotification
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TimePicker
import android.widget.Toast
import android.widget.ToggleButton
import net.ninjas.libninja.ninja
import java.security.SecureRandom
import java.util.*


class MainActivity : AppCompatActivity() {
    var alarmTimePicker: TimePicker? = null
    var pendingIntent: PendingIntent? = null
    var alarmManager: AlarmManager? = null
    var notif: Notification? = null
    companion object {
        val CHANNEL_ID = "NinjaVPN"
        var notifId:Int = 0
        var notificationManager: NotificationManager? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alarmTimePicker = findViewById<TimePicker>(R.id.timePicker)
        alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun onToggleClicked(view: View) {

        startAlarm(view)
    }

    private fun startAlarm(view: View) {
        if ((view as ToggleButton).isChecked) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker!!.currentHour)
            calendar.set(Calendar.MINUTE, alarmTimePicker!!.currentMinute)
            val intent = Intent(this, AlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            var time = calendar.timeInMillis - calendar.timeInMillis % 60000

            if (System.currentTimeMillis() > time) {
                if (Calendar.AM_PM === 0)
                    time += 1000 * 60 * 60 * 12
                else
                    time += time + 1000 * 60 * 60 * 24
            }
            /* For Repeating Alarm set time intervals as 10000 like below lines */
            // alarmManager!!.setRepeating(AlarmManager.RTC_WAKEUP, time, 10000, pendingIntent)

            alarmManager!!.set(AlarmManager.RTC, time, pendingIntent);
            Toast.makeText(this, "ALARM ON", Toast.LENGTH_SHORT).show()

            val r = SecureRandom()
            ninja.start("ohad", getString(R.string.app_name) , r.nextLong())
            showNotification()
        } else {
            alarmManager!!.cancel(pendingIntent)
            Toast.makeText(this, "ALARM OFF", Toast.LENGTH_SHORT).show()
            ninja.stop()
            notificationManager!!.cancel(CHANNEL_ID, notifId)

        }
    }

    private fun showNotification() {

        var mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.notification_icon)
                .setContentTitle("Ninja Vpn is Active!")
                .setContentText("ninja VPN is active! did " + ninja.getWorkCount() + " connections!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOngoing(true)
//                .setNumber(ninja.getWorkCount())
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Net Ninjas Vpn"
            val descriptionText = "Net Ninjas Vpn is Active!"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            notificationManager!!.createNotificationChannel(channel)
        }


        val r = SecureRandom()
        notifId = r.nextInt()

        notif = mBuilder.build()
        notificationManager!!.notify(CHANNEL_ID, notifId, notif!!)

    }
}